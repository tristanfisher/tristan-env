#!/usr/bin/env bash

FILE_NAME=$1

openssl genrsa -des3 -out $FILE_NAME.key 2048
openssl req -new -key $FILE_NAME.key -out $FILE_NAME.csr -subj "/C=US/ST=New York/L=New York City/O=Example./OU=Nerds/CN=example.org"

#take off password
cp $FILE_NAME.key $FILE_NAME.key.org
openssl rsa -in $FILE_NAME.key.org -out $FILE_NAME.key

openssl x509 -req -days 1095 -in $FILE_NAME.csr -signkey $FILE_NAME.key -out $FILE_NAME.crt
