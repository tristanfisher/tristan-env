#!//bin/bash
#whois for a domain.  spit out the important dates

qwhois(){
	response=$(whois "$1" | egrep 'Creation Date|Updated Date|Expiration Date')
	if [[ -z "$response" ]]
	then
		echo "No response. $1"
	else
	echo "$1"
	echo "$response"
	fi
}

while true
do
	read -p ">> " domain
	if [[ -z "$domain" ]]; then
		exit 0
	else
		qwhois "$domain"
	fi
done


