set nocompatible				" vim, not vi
set showcmd						" Show (partial) command in status line.
set showmode
set showmatch       " Show matching brackets.

"filetype off 					" required for /
set encoding=utf-8				" set file encoding
set ruler
set visualbell t_vb=
set number
set ttyfast

set ls=2
set scrolloff=3
set scrolljump=3

" default width/tab
set shiftwidth=4
set tabstop=4
set expandtab
"set smarttab

set smartcase " Do smart case matching
set autowrite       " Automatically save before commands like :next and :make
set smartindent
set autoindent

set nohidden
set background=dark
"let g:solarized_termcolors=256
"colorscheme solarized

"incremental search and highlight the target
set hls
set hlsearch
set incsearch
let g:HLSpace = 1
set ignorecase
highlight Search cterm=underline gui=underline ctermbg=none guibg=none ctermfg=none guifg=none

function ToggleSpaceUnderscoring()
    if g:HLSpace
        highlight Search cterm=underline gui=underline ctermbg=none guibg=none ctermfg=none guifg=none
        let @/ = " "
    else
        highlight clear
        silent colorscheme "".g:HLColorScheme
        let @/ = ""
    endif
    let g:HLSpace = !g:HLSpace
endfunction

" Use english for spellchecking, but don't spellcheck by default
if version >= 700
   set spl=en spell
   set nospell
endif

"status bar
set laststatus=2
set statusline=                                   "clear out the statusline
"set statusline=%t                                "just the filename
set statusline+=%f                                 "filename as relative to where vim was invoked
set statusline+=\ %y                              "file type (py/c/rb)
set statusline+=\ [%{strlen(&fenc)?&fenc:'none'}, "file encoding
set statusline+=%{&ff}]                           "file format
set statusline+=\ \ \ \ \ \ %h                    "help file flag
set statusline+=%m                                "modified flag
set statusline+=%r                                "read only flag if that's the case on the open file
set statusline+=%2c                               "cursor column
set statusline+=[%4l/%4L]                         "cursor line/total lines with at least a 4 char width
set statusline+=\ [%P]                            "cursor percent through file

" Remove any trailing whitespace that is in the file
autocmd BufRead,BufWrite * if ! &bin | silent! %s/\s\+$//ge | endif


" Restore cursor position to where it was before exiting vim
augroup JumpCursorOnEdit
   au!
   autocmd BufReadPost *
            \ if expand("<afile>:p:h") !=? $TEMP |
            \   if line("'\"") > 1 && line("'\"") <= line("$") |
            \     let JumpCursorOnEdit_foo = line("'\"") |
            \     let b:doopenfold = 1 |
            \     if (foldlevel(JumpCursorOnEdit_foo) > foldlevel(JumpCursorOnEdit_foo - 1)) |
            \        let JumpCursorOnEdit_foo = JumpCursorOnEdit_foo - 1 |
            \        let b:doopenfold = 2 |
            \     endif |
            \     exe JumpCursorOnEdit_foo |
            \   endif |
            \ endif
   " Need to postpone using "zv" until after reading the modelines.
   autocmd BufWinEnter *
            \ if exists("b:doopenfold") |
            \   exe "normal zv" |
            \   if(b:doopenfold > 1) |
            \       exe  "+".1 |
            \   endif |
            \   unlet b:doopenfold |
            \ endif
augroup END


execute pathogen#infect()

let mapleader = ","
nnoremap ; :

" key shortcuts
nnoremap <F2> :set invpaste paste?<CR>
map <F5> <ESC>:NERDTreeToggle<RETURN>
nmap <F6> :set number! number?<cr>

"insert blanklines and stay in normal mode"
nnoremap <silent> zj o<Esc>
nnoremap <silent> zk O<Esc>

nmap <leader>a <Esc>:Ack!
map <leader>j :RopeGotoDefinition<CR>
map <leader>r :RopeRename<CR>

map <leader>td <Plug>TaskList
map <leader>g :GundoToggle<CR>
" Open Url on this line with the browser \w
map <Leader>w :call Browser ()<CR>

" Code folding.  Hit <space> to fold.
nnoremap <space> za

set foldmethod=indent
set foldlevel=99

" General syntax and config checkers
syntax on           " syntax highlighing
syntax enable
filetype on           " try to detect filetypes

highlight MatchParen ctermbg=5

"change statusline colors based on insert/normal mode status
if version >= 700
  au InsertEnter * hi StatusLine term=reverse ctermbg=5 gui=undercurl guisp=Magenta
  au InsertLeave * hi StatusLine term=reverse ctermfg=0 ctermbg=2 gui=bold,reverse
endif

"{{{Taglist configuration
let Tlist_Use_Right_Window = 1
let Tlist_Enable_Fold_Column = 0
let Tlist_Exit_OnlyWindow = 1
let Tlist_Use_SingleClick = 1
let Tlist_Inc_Winwidth = 0

" Open the TagList Plugin <F3>
nnoremap <silent> <F3> :Tlist<CR>
" Next Tab
nnoremap <silent> <C-Right> :tabnext<CR>
" Previous Tab
nnoremap <silent> <C-Left> :tabprevious<CR>
" New Tab
nnoremap <silent> <C-t> :tabnew<CR>

nnoremap <silent> <Home> i <Esc>r
nnoremap <silent> <End> a <Esc>r

let g:syntastic_json_checkers=['jsonlint']
"todo: consier switching set to setlocal
au! BufRead,BufNewFile *.c set filetype=c
augroup cpp_autocmd
	 " Remove ALL autocommands for the current group.
	autocmd!
	autocmd FileType c set makeprg=gcc\ %
	autocmd FileType c set expandtab
	autocmd FileType c set foldmethod=syntax
augroup END

au! BufRead,BufNewFile *.cpp set filetype=cpp
au! BufRead,BufNewFile *.cc set filetype=cpp
augroup cpp_autocmd
	 " Remove ALL autocommands for the current group.
	autocmd!
	autocmd FileType cpp set makeprg=g++\ %
	autocmd FileType cpp set expandtab
	autocmd FileType cpp set foldmethod=syntax
augroup END

au! BufRead,BufNewFile *.json set filetype=json
augroup json_autocmd
  autocmd!
  autocmd FileType json set autoindent
  autocmd FileType json set formatoptions=tcq2l
  autocmd FileType json set textwidth=78 shiftwidth=2
  autocmd FileType json set softtabstop=2 tabstop=8
  autocmd FileType json set expandtab
  autocmd FileType json set foldmethod=syntax
augroup END

"
" Python
"

au! BufRead,BufNewFile *.py set filetype=python
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h set textwidth=120
augroup py_autocmd
  autocmd!
  autocmd FileType python set expandtab
  autocmd FileType python set foldmethod=indent
  autocmd FileType python set shiftwidth=4
  autocmd FileType python set tabstop=4
  autocmd FileType python set softtabstop=4
augroup END

if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
  \| exe "normal g'\"" | endif
  autocmd FileType python set omnifunc=pythoncomplete#Complete
  filetype on
  filetype plugin on
  filetype indent on
endif



" Number of spaces that a pre-existing tab is equal to.
" For the amount of space used for a new tab use shiftwidth.
"au BufRead,BufNewFile *py,*pyw,*.c,*.h set tabstop=8
" What to use for an indent.
" This will affect Ctrl-T and 'autoindent'.
" Python: 4 spaces
" C: tabs (pre-existing files) or 4 spaces (new files)
"au BufRead,BufNewFile *.py,*pyw set shiftwidth=4
"au BufRead,BufNewFile *.py,*.pyw set expandtab
"fu Select_c_style()
"    if search('^\t', 'n', 150)
"        set shiftwidth=8
"        set noexpandtab
"    el
"        set shiftwidth=4
"        set expandtab
"    en
"endf
"au BufRead,BufNewFile *.c,*.h call Select_c_style()
"au BufRead,BufNewFile Makefile* set noexpandtab

let g:pyflakes_use_quickfix = 0

"https://github.com/scrooloose/syntastic
let g:syntastic_python_checkers = ['pylint']

"hit <leader>pw when our cursor is on a module
let g:pep8_map='<leader>8'

"use unix line endings
au BufNewFile *.py,*.pyw,*.c,*.h set fileformat=unix

" Uncomment to flag tabs at start of line as bad
"au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

"
" checkout :highlight to see what you can override
"

highlight BadWhitespace ctermbg=cyan guibg=#AACCDD
highlight SpellBad ctermbg=8 guibg=#EEEEEE

"maybe checkout supertab or a tab completion and remap it from <tab>

"py.test
" Execute the tests
nmap <silent><Leader>tf <Esc>:Pytest file<CR>
nmap <silent><Leader>tc <Esc>:Pytest class<CR>
nmap <silent><Leader>tm <Esc>:Pytest method<CR>
" cycle through test errors
nmap <silent><Leader>tn <Esc>:Pytest next<CR>
nmap <silent><Leader>tp <Esc>:Pytest previous<CR>
nmap <silent><Leader>te <Esc>:Pytest error<CR>

" Add the virtualenv's site-packages to vim path
" if version >= 700
" TODO: fix, causing issues on debian 7.4
"py << EOF
"import os.path
"import sys
"import vim
"if 'VIRTUAL_ENV' in os.environ:
"    project_base_dir = os.environ['VIRTUAL_ENV']
"    sys.path.insert(0, project_base_dir)
"    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"    execfile(activate_this, dict(__file__=activate_this))
"EOF


" controlp edits
"hide files we're not going to edit:
:let g:ctrlp_custom_ignore = '\v\~$|\.(avi|mp4|o|swp|pyc|wav|mp3|ogg|blend)$|(^|[/\\])\.(hg|git|bzr)($|[/\\])|__init__\.py'
:let g:ctrlp_working_path_mode = 0
:let g:ctrlp_dotfiles = 0
:let g:ctrlp_switch_buffer = 0

