#!/usr/bin/env bash

VOLUME=$1
AMI=$2
#DIR=$#
DIR='/dev/sdh'

if [ -z "$VOLUME" ] || [ -z "$AMI" } 
then 
  echo "Usage: connect_volume_to_ami.sh volume_identifier ami_identifier"
  exit 1
fi

ec2-attach-volume $VOLUME -i $AMI -d $DIR

