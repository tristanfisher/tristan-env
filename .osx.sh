# Enable full keyboard access for all controls (e.g. enable Tab in modal dialogs)
#defaults write NSGlobalDomain AppleKeyboardUIMode -int 3

# save screen grabs as png
defaults write com.apple.screencapture type png

# Disable menu bar transparency
defaults write -g AppleEnableMenuBarTransparency -bool false

# Expand save panel by default
defaults write -g NSNavPanelExpandedStateForSaveMode -bool true

# Expand print panel by default
defaults write -g PMPrintingExpandedStateForPrint -bool true

# Disable shadow in screenshots
defaults write com.apple.screencapture disable-shadow -bool true

# Enable highlight hover effect for the grid view of a stack (Dock)
defaults write com.apple.dock mouse-over-hilte-stack -bool true

# Disable press-and-hold for keys in favor of key repeat
defaults write -g ApplePressAndHoldEnabled -bool true

#Make key repeat actually (somewhat) quick.
defaults write NSGlobalDomain KeyRepeat -int 5
defaults write NSGlobalDomain InitialKeyRepeat -int 11

# Disable auto-correct
#defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

# Disable window animations
defaults write NSGlobalDomain NSAutomaticWindowAnimationsEnabled -bool false

# Automatically open a new Finder window when a volume is mounted
defaults write com.apple.frameworks.diskimages auto-open-ro-root -bool true
defaults write com.apple.frameworks.diskimages auto-open-rw-root -bool true

# Avoid creating .DS_Store files on network volumes
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true

# Disable Safari’s thumbnail cache for History and Top Sites
defaults write com.apple.Safari DebugSnapshotsUpdatePolicy -int 2

# Remove useless icons from Safari’s bookmarks bar
defaults write com.apple.Safari ProxiesInBookmarksBar "()"

# Enable Safari Debug menu
defaults write com.apple.Safari IncludeInternalDebugMenu 1

# Disable send and reply animations in Mail.app
defaults write com.apple.Mail DisableReplyAnimations -bool true
defaults write com.apple.Mail DisableSendAnimations -bool true

# Disable animations in switching to misson control:
defaults write com.apple.dock expose-animation-duration -float 0.1; killall Dock

# Disable Resume system-wide
defaults write NSGlobalDomain NSQuitAlwaysKeepsWindows -bool false

# Enable Dashboard dev mode (allows keeping widgets on the desktop)
defaults write com.apple.dashboard devmode -bool true

# Reset Launchpad
rm ~/Library/Application\ Support/Dock/*.db

# Show the ~/Library folder
chflags nohidden ~/Library
chflags nohidden ~/usr
chflags nohidden ~/tmp

# Fix for the ancient UTF-8 bug in QuickLook (http://mths.be/bbo)
echo "0x08000100:0" > ~/.CFUserTextEncoding

# Show full path in Finder title bar
defaults write com.apple.finder _FXShowPosixPathInTitle -bool YES

# Show the Dock on the top-left
#defaults write com.apple.dock pinning start
#defaults write com.apple.dock orientation left

# Use "suck" minimization effect
defaults write com.apple.dock mineffect suck

# Hide iTunes Store arrow links
defaults write com.apple.iTunes show-store-arrow-links -bool FALSE

# Hide iTunes Ping dropdown
defaults write com.apple.iTunes hide-ping-dropdown -bool TRUE

# Make iTunes window controls horizontal again
defaults write com.apple.iTunes full-window -boolean YES

# Make Dock icons translucent for hiddens apps
defaults write com.apple.Dock showhidden -bool YES

# Stop prompting with time machine for new drives
defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true

#Disable 3d effect on dock
defaults write com.apple.dock no-glass -boolean YES;
#to bring it back:  defaults write com.apple.dock no-glass -bool NO

#disable bouncing icons:
#defaults write com.apple.dock no-bouncing -bool True

#Make dock come out to play faster:
defaults write com.apple.Dock autohide-delay -float 0;
#killall dock

#add a recent applications stack:
defaults write com.apple.dock persistent-others -array-add '{ "tile-data" = { "list-type" = 1; }; "tile-type" = "recents-tile"; }'
#highlight stack selection with a background:
defaults write com.apple.dock mouse-over-hilte-stack -boolean YES;

#disable [slow] launchpad effects -- makes switching desktops faster
#caution: using an int here leaves a weird animation fragment delay
defaults write com.apple.dock springboard-show-duration -float 0.1;
defaults write com.apple.dock springboard-hide-duration -float 0.1;
defaults write com.apple.dock expose-animation-duration -float 0.1; killall Dock;

#default to icloud? seriously?
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false;

# Kill affected applications
for app in Safari Finder Dock Mail iTunes SystemUIServer; do killall "$app"; done

#Get rid of the notification center.  What a silly idea to not only blast the user with notificiations
#but also track application alerts in a method that destroys all context.
launchctl unload -w /System/Library/LaunchAgents/com.apple.notificationcenterui.plist
killall NotificationCenter
sudo defaults write /System/Library/LaunchAgents/com.apple.notificationcenterui KeepAlive -bool False
