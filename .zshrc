# Path to your oh-my-zsh configuration.
# curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="wedisagree"
#cypher is cool, but i like seeing my git branch
#ZSH_THEME="cypher"
ZSH_THEME="jnrowe"

zstyle ':completion:*:functions' ignored-patterns '_*'

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git)

source $ZSH/oh-my-zsh.sh
DISABLE_CORRECTION="true"
unsetopt correct_all
unsetopt correct


# Customize to your needs...
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin:/usr/local/git/bin:/opt/local/bin

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

source $HOME/.zsh_tristan
if [ -f $HOME/.zsh_employer ]; then
  source $HOME/.zsh_employer
fi

if [ -f $HOME/.zsh_api ]; then
	source $HOME/.zsh_api
fi

if [ -f $HOME/.zsh_cpp ]; then
	source $HOME/.zsh_cpp
fi

#get out of my path, rvm
#PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

